# Simple golang application

[Application source](https://gowebexamples.com/json/)

    $ go run json.go
    
    $ curl -s -XPOST -d'{"firstname":"Donald","lastname":"Trump","age":70}' http://localhost:8080/decode
    Donald Trump is 70 years old!
    
    $ curl -s http://localhost:8080/encode
    {"firstname":"John","lastname":"Doe","age":25}
